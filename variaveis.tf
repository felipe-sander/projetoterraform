variable "aws_region" {
    description = "AWS region"
    default     = "us-east-1"
    type        = "string" 
}

variable "acess_key" {
    description = "Your AWS acess key"
    default     = "******************"
    type        = "string" 
}

variable "secret_key" {
    description = "Your AWS secret key"
    default     = "*******************"
    type        = "string" 
}

variable "vpc_name" {
    description = "Your AWS vpc name"
    default     = "Minha Vpc"
    type        = "string" 
}

variable "vpc_cidr" {
    description = "Your AWS vpc cidr"
    default     = "10.0.0.0/16"
    type        = "string" 
}

variable "vpc_azs" {
    description = "Your AWS vpc azs"
    default     = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
    type        = "list" 
}

variable "vpc_private_subnets" {
    description = "Your AWS vpc subnets private"
    default     = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
    type        = "list" 
}

variable "vpc_public_subnets" {
    description = "Your AWS vpc subnets public"
    default     = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]
    type        = "list" 
}

variable "vpc_enable_nat_gateway" {
    description = "Your AWS vpc nat gateway enable"
    default     = "true"
    type        = "string" 
}

variable "vpc_tags" {
    description = "Your AWS vpc tags"
    name        = "Minha Tag"
    default     = "true"
    type        = "string"
     
}
